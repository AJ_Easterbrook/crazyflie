#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/exalabs5/crazyflie_ws/devel:$CMAKE_PREFIX_PATH"
export PWD="/home/exalabs5/crazyflie_ws/build"
export ROSLISP_PACKAGE_DIRECTORIES="/home/exalabs5/crazyflie_ws/devel/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/exalabs5/crazyflie_ws/src:/home/exalabs5/bebop_ws/src/bebop_autonomy/bebop_autonomy:/home/exalabs5/bebop_ws/src/bebop_autonomy/bebop_description:/home/exalabs5/bebop_ws/src/bebop_autonomy/bebop_msgs:/home/exalabs5/bebop_ws/src/bebop_autonomy/bebop_driver:/home/exalabs5/bebop_ws/src/bebop_autonomy/bebop_tools:/opt/ros/indigo/share:/opt/ros/indigo/stacks"